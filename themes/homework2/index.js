const themeSwitch = document.querySelectorAll('.change-theme');
const themeSelector = document.querySelector('[title="themes"]');

themeSwitch.forEach(switcher => {
  switcher.addEventListener('click', function (event) {
    const selectedTheme = this.dataset.theme;
    addThemeToLocalStorage(selectedTheme);
    setTheme(selectedTheme);
  });
});

function addThemeToLocalStorage(theme) {
  localStorage.setItem('selectedTheme', theme);
}

function setTheme(theme) {
  let themeurl = `css/${theme}.css`;
  themeSelector.setAttribute('href', themeurl);
}

// проверяем, есть ли сохраненная тема в localStorage
const savedTheme = localStorage.getItem('selectedTheme');
if (savedTheme) {
  setTheme(savedTheme);
}